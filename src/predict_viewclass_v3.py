from __future__ import division, print_function, absolute_import
import numpy as np
import tensorflow as tf
import random
import sys
import cv2
import dicom
import os
sys.path.append('./funcs/')
sys.path.append('./nets/')
import subprocess
import time
from shutil import rmtree
from optparse import OptionParser
from scipy.misc import imread
from echoanalysis_tools import output_imgdict
import pickle
# # Hyperparams
parser=OptionParser()
parser.add_option("-d", "--dicomdir", dest="dicomdir", help = "dicomdir")
parser.add_option("-g", "--gpu", dest="gpu", default = "0", help = "cuda device to use")
parser.add_option("-m", "--model", dest="model")
params, args = parser.parse_args()
dicomdir = params.dicomdir
model = params.model

import vgg as network
import pandas as pd
os.environ["CUDA_VISIBLE_DEVICES"] = params.gpu



def extract_imgs_from_dicom(dicomdir, dicomfile, out_directory):
    """
    Extracts jpg images from DCM files in the given directory

    @param directory: folder with DCM files of echos
    @param out_directory: destination folder to where converted jpg files are placed
    @param target: destination folder to where converted jpg files are placed
    """
    filename=dicomdir+'/'+dicomfile
    print(filename)
    ds = dicom.read_file(filename,            force=True)
    #print("Frames: %i"%ds.NumberOfFrames)
    if ("NumberOfFrames" in  dir(ds)) and (ds.NumberOfFrames>1):
        m = random.sample(range(1, ds.NumberOfFrames), 10)
        for n in m:
            outfilename = 'image/'+dicomfile + str(n)
            print(outfilename)
            command = 'dcmj2pnm --frame ' + str(n) + " "+os.path.join(dicomdir,dicomfile) + " " + os.path.join(dicomdir,outfilename+ '.pnm')
            print(command)
            subprocess.call(command, shell=True)
            filesize = os.stat(os.path.join(dicomdir,dicomfile)).st_size
            print(filesize)
            print(os.path.join(dicomdir, outfilename+ '.pnm'))
            targetimage=cv2.imread(os.path.join(dicomdir, outfilename+ '.pnm'),0)

            cv2.imwrite(os.path.join(dicomdir, outfilename+ '.jpg'), cv2.resize(targetimage, (224, 224)), [cv2.IMWRITE_JPEG_QUALITY, 95])
            os.remove(os.path.join(dicomdir, outfilename+ '.pnm'))
    return 1


def classify(directory, feature_dim, label_dim, model_name):
    """
    Classifies echo images in given directory
    print directory
    @param directory: folder with jpg echo images for classification
    """
    imagedict = {}
    predictions = {}
    for filename in os.listdir(directory):
        if "jpg" in filename:
            image = imread(directory + filename, flatten = True).astype('uint8')
            imagedict[filename] = [image.reshape((224,224,1))]

    tf.reset_default_graph()
    sess = tf.Session()
    model = network.Network(0.0, 0.0, feature_dim, label_dim, False)
    sess.run(tf.global_variables_initializer())

    saver = tf.train.Saver()
    saver.restore(sess, model_name)

    for filename in imagedict:
        predictions[filename] =np.around(model.probabilities(sess, \
                  imagedict[filename]), decimals = 3)

    return predictions


def main():
    model = "view_23_e5_class_11-Mar-2018"
    dicomdir = "dicomsample"
    model_name = '/root/echocv/models/' + model

    infile = open("viewclasses_" + model + ".txt")
    infile = infile.readlines()
    views = [i.rstrip() for i in infile]

    feature_dim = 1
    label_dim = len(views)
    view_prob_file=model + "_" + dicomdir  + "_probabilities.txt"
    if not os.path.exists(view_prob_file):
        out = open(view_prob_file, 'w')
        out.write("study\timage")
        for j in views:
            #print(j)
            out.write("\t" + "prob_" + j)
        out.write('\n')
    else:
        out = open(view_prob_file, 'a')
    x = time.time()
    temp_image_directory = dicomdir + '/image/'
        


    df_results=pd.DataFrame(columns=['File','Classification','Prob'])
    # This would print all the files and directories
    print(dicomdir)
    predictprobdict = {}
    for dicomfile in os.listdir(dicomdir):
        print(dicomfile)
        if os.path.exists(temp_image_directory):
            rmtree(temp_image_directory)
            os.makedirs(temp_image_directory)
        fullpath=dicomdir+'/'+dicomfile
        print(fullpath)
        if os.path.isfile(fullpath):
            predictions_file='results/'+dicomfile+'.viewclass_predictions.p'
            if not os.path.exists(predictions_file):
                extract_imgs_from_dicom(dicomdir,dicomfile, temp_image_directory)
                predictions = classify(temp_image_directory, feature_dim, label_dim, model_name)
                pickle.dump(predictions,open( predictions_file, "wb" ))
            else:
                predictions =  pickle.load( open( predictions_file, "rb" ))
                print("Loaded prections from %s"%predictions_file)

            print("predictions: %i"%len(predictions))
            for image in predictions.keys():
                prefix = image.split(".dcm")[0] + ".dcm"
                print("Image: "+image + " Prefix: "+prefix)
                if not predictprobdict.has_key(prefix):
                    predictprobdict[prefix] = []
                    print(predictions[image][0])
                    predictprobdict[prefix].append(predictions[image][0])

    for prefix in predictprobdict.keys():
        predictprobmean =  np.mean(predictprobdict[prefix], axis = 0)
        viewmaxprob=views[np.argmax(predictprobmean)]
        print(prefix,np.max(predictprobmean),viewmaxprob)
        df_results=df_results.append({'File':prefix,'Classification':viewmaxprob,'Prob':np.max(predictprobmean)},ignore_index=True)
        out.write(dicomdir + "\t" + prefix)
        for i in predictprobmean:
            out.write("\t" + str(i))
        out.write( "\n")
    y = time.time()


    #pickle.dump(df_results, open( "df_results.p", "wb" ) )
    out.close()
    if os.path.exists(temp_image_directory):
        rmtree(temp_image_directory)
        print("removed temporary images")
    return df_results

if __name__ == '__main__':
    main()
