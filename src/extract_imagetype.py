import dicom
import openpyxl
from openpyxl import load_workbook,Workbook
import fnmatch
import os

VIEW="US_CA_LVDTI_EA_DTI_LATERAL"
PATH='/root/echocv/dicomsample/'

matches = []
for root, dirnames, filenames in os.walk(PATH+"/"+VIEW):
    for filename in fnmatch.filter(filenames, '*.*'):
        matches.append(os.path.join(root, filename))


print len(matches)
XLS_FILE="/root/echocv/Views.xlsx"
#wb=Workbook()
#wb.save(XLS_FILE)
wb=load_workbook(XLS_FILE)
ws=wb.create_sheet(VIEW)
ws.append(['FileName','PatientID','StudyDescription','ImageType','Region Spatial Format','Region Data Type','Region Flags','Physical Delta Y','Physical Unit Y'])

for result in matches:
    print(result)
    #print file.Folder, file.FileName
    #src_file="D:\\ArchiveFolder\\WorkingArchive\\"+file.Folder+"\\"+file.FileName
    try:
        ds = dicom.read_file(result)
        rsf= ds[0x0018,0x6011][0][0x0018,0x6012].value
        rdt= ds[0x0018,0x6011][0][0x0018,0x6014].value
        rf= ds[0x0018,0x6011][0][0x0018,0x6016].value
        phdy= ds[0x0018,0x6011][0][0x0018,0x602C].value
        phuy= ds[0x0018,0x6011][0][0x0018,0x6026].value


        print(rsf,rdt,rf)
        row=[result,ds.PatientID,ds.StudyDescription, str(ds.ImageType),rsf,rdt,rf,phdy,phuy]

        print(row)
        ws.append(row)
    except Exception, e:
        print e
    wb.save(XLS_FILE)

wb.save(XLS_FILE)
