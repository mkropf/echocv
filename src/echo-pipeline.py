import predict_viewclass_v3
import segment_a4c_a2c_a3c_plax_psax
import analyze_segmentations_unet_a4c_a2c_psax_plax_v2
import strain_strainrate_unet_pool_v2
from timer import Timer

#TODO: copy file to dicomsample folder or change file processing function
with Timer('Predict viewclass'):
    df=predict_viewclass_v3.main()
    print(df)

with Timer('Segmentation'):
    segment_a4c_a2c_a3c_plax_psax.main()

with Timer('Conventional Measurements'):
    analyze_segmentations_unet_a4c_a2c_psax_plax_v2.main()

with Timer('Speckle Tracking'):
    strain_strainrate_unet_pool_v2.main()

