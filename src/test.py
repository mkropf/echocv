import dicom
import subprocess
import os
from echoanalysis_tools import output_imgdict

targetfile='dicomsample/IMG00090768_2833-00004_Baseline.dcm'
ds = dicom.read_file(targetfile, force = True)
if ("NumberOfFrames" in  dir(ds)) and (ds.NumberOfFrames>1):
    outrawfile = targetfile + "_raw"
    command = 'gdcmconv -w ' + targetfile + " "  + outrawfile
    subprocess.call(command, shell=True)
   # time.sleep(10)
    if os.path.exists(outrawfile):
        ds = dicom.read_file(outrawfile, force = True)
        imgdict = output_imgdict(ds)
        print(imgdict)