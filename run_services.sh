#!/bin/bash

jupyter notebook --ip 0.0.0.0 --no-browser &
tensorboard --host 0.0.0.0 --logdir=/tmp 
