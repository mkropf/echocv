FROM floydhub/dl-docker:cpu
COPY src/requirements.txt /root/echocv/requirements.txt

ENV http_proxy http://proxy.charite.de:8080
ENV https_proxy https://proxy.charite.de:8080

RUN apt-get install -y libgdcm-tools dcmtk
WORKDIR /root/echocv
RUN pip install -r requirements.txt
COPY run_services.sh /root/
CMD /root/run_services.sh
EXPOSE 6006 8888
